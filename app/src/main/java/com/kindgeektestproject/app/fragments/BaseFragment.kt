package com.kindgeektestproject.app.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.annotation.VisibleForTesting
import android.support.v4.app.Fragment
import androidx.navigation.fragment.findNavController
import com.kindgeektestproject.app.activity.BaseActivity
import com.kindgeektestproject.app.viewmodel.BaseViewModel
import com.kindgeektestproject.data.entities.common.State
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 12:43
 */
abstract class BaseFragment : Fragment() {
    @Inject
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public lateinit var viewModelFactory: ViewModelProvider.Factory
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public val baseActivity: BaseActivity
        get() = activity as BaseActivity

    abstract fun subscribeToModel()
    abstract fun initViewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        subscribeToModel()
    }

    protected fun <T : BaseViewModel> subscribeToViewModelEvents(viewModel: T) {
        viewModel.snackbarEvent.observe(this, Observer { showSnackbar(it) })
        viewModel.loadingEvent.observe(this, Observer { processResponseStatus(State.LOADING) })
        viewModel.successEvent.observe(this, Observer { processResponseStatus(State.SUCCESS) })
        viewModel.errorEvent.observe(this, Observer { processResponseStatus(State.ERROR(it)) })
    }

    public fun showSnackbar(@StringRes stringRes: Int? = -1, message: String? = null) {
        baseActivity.showSnackbar(stringRes, message)
    }

    public fun showLoading() {
        baseActivity.showLoading()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public fun hideLoading() {
        baseActivity.hideLoading()
    }

    public fun navController() = findNavController()

    private fun processResponseStatus(state: State?) {
        baseActivity.processResponseStatus(state)
    }
}