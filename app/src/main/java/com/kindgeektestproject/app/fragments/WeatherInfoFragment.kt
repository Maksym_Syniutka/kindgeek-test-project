package com.kindgeektestproject.app.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.databinding.FragmentWeatherInfoBinding
import com.kindgeektestproject.app.di.Injectable
import com.kindgeektestproject.app.utils.ResourceObserver
import com.kindgeektestproject.app.utils.extensions.getSharedViewModel
import com.kindgeektestproject.app.viewmodel.SharedViewModel

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 08:48
 */
class WeatherInfoFragment : BaseFragment(), Injectable {

    private lateinit var binding: FragmentWeatherInfoBinding
    lateinit var viewModel: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather_info, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getWeatherInfo()
    }

    override fun subscribeToModel() {
        subscribeToViewModelEvents(viewModel)
        viewModel.weatherInfoLiveData.observe(this, ResourceObserver(
            onSuccess = { binding.weatherInfo = it },
            onError = {}
        ))
    }

    override fun initViewModel() {
        viewModel = getSharedViewModel(baseActivity, viewModelFactory)
    }
}