package com.kindgeektestproject.app.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.databinding.FragmentMapBinding
import com.kindgeektestproject.app.di.Injectable
import com.kindgeektestproject.app.utils.ResourceObserver
import com.kindgeektestproject.app.utils.extensions.getSharedViewModel
import com.kindgeektestproject.app.viewmodel.SharedViewModel


/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 09:50
 */
class MapFragment : BaseFragment(), Injectable, OnMapReadyCallback, GoogleMap.OnMapLongClickListener {

    private var binding: FragmentMapBinding? = null
    private var currentMarker: LatLng? = null
    private var googleMap: GoogleMap? = null
    lateinit var viewModel: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.map?.apply {
            onCreate(savedInstanceState)
            onResume()
            getMapAsync(this@MapFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        binding?.map?.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding?.map?.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding?.map?.onLowMemory()
    }

    override fun onDestroy() {
        binding?.map?.onDestroy()

        googleMap?.clear()
        googleMap = null

        super.onDestroy()
    }

    override fun subscribeToModel() {
        subscribeToViewModelEvents(viewModel)
        viewModel.weatherInfoLiveData.observe(this, ResourceObserver(
            onSuccess = { weatherInfo ->
                weatherInfo?.let { currentMarker = LatLng(it.latitude, it.longitude) }
            },
            onError = {},
            onLoading = {}
        ))
    }

    override fun initViewModel() {
        viewModel = getSharedViewModel(baseActivity, viewModelFactory)
    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0
        googleMap?.setOnMapLongClickListener(this)

        currentMarker?.let {
            val bounds = LatLngBounds.Builder().include(it).build()
            googleMap?.apply {
                moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.center, 5f))
                addMarker(MarkerOptions().position(it))
                setOnMapLoadedCallback {
                    animateCamera(CameraUpdateFactory.newLatLngZoom(bounds.center, 10f))
                }
            }
        }
    }

    override fun onMapLongClick(position: LatLng?) {
        position?.let {
            googleMap?.apply {
                clear()
                addMarker(MarkerOptions().position(it))
            }
            viewModel.updateWeatherInfo(it)
        }
    }
}