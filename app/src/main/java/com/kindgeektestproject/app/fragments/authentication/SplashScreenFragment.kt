package com.kindgeektestproject.app.fragments.authentication

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.activity.MainActivity
import com.kindgeektestproject.app.databinding.FragmentSplashScreenBinding
import com.kindgeektestproject.app.di.Injectable
import com.kindgeektestproject.app.fragments.BaseFragment
import com.kindgeektestproject.app.utils.ResourceObserver
import com.kindgeektestproject.app.utils.extensions.getViewModel
import com.kindgeektestproject.app.utils.extensions.isConnected
import com.kindgeektestproject.app.utils.extensions.startActivity
import com.kindgeektestproject.app.viewmodel.SplashScreenFragmentViewModel
import com.kindgeektestproject.data.exceptions.WeatherInfoDoesNotExist

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 13:49
 */
class SplashScreenFragment : BaseFragment(), Injectable {

    private lateinit var binding: FragmentSplashScreenBinding
    lateinit var viewModel: SplashScreenFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash_screen, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getWeatherInfo()
    }

    override fun subscribeToModel() {
        subscribeToViewModelEvents(viewModel)
        viewModel.weatherInfoLiveData.observe(this, ResourceObserver(
            onSuccess = {
                context?.startActivity<MainActivity>()
            },
            onError = {
                when (it) {
                    is WeatherInfoDoesNotExist -> {
                        if (context.isConnected()) {
                            context?.startActivity<MainActivity>()
                        } else {
                            context?.let { safeContext ->
                                AlertDialog.Builder(safeContext)
                                    .setTitle(R.string.splash_screen_fragment_lost_internet_connection_dialog_title)
                                    .setMessage(R.string.splash_screen_fragment_lost_internet_connection_dialog_message)
                                    .setPositiveButton(R.string.splash_screen_fragment_lost_internet_connection_dialog_positive_btn_title) { _, _ -> baseActivity.finishAffinity() }
                                    .show()
                            }
                        }
                    }
                    else -> showSnackbar(message = it?.message)
                }
            },
            onLoading = {}
        ))
    }

    override fun initViewModel() {
        viewModel = getViewModel(viewModelFactory)
    }
}