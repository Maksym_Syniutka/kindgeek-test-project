package com.kindgeektestproject.app.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import com.kindgeektestproject.data.db.WeatherDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:24
 */
@Module
class DatabaseModule {
    @Module
    companion object {
        @Provides
        @Singleton
        @JvmStatic
        fun provideWeatherDatabase(application: Application): WeatherDatabase {
            return Room
                .databaseBuilder(application, WeatherDatabase::class.java, "weather.db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}