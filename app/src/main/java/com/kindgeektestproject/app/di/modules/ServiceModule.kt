package com.kindgeektestproject.app.di.modules

import com.kindgeektestproject.data.services.db.IWeatherDbService
import com.kindgeektestproject.data.services.db.WeatherDbServiceImpl
import dagger.Binds
import dagger.Module

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:07
 */
@Module

abstract class ServiceModule {

    @Binds
    abstract fun bindWeatherDbService(weatherDbServiceImpl: WeatherDbServiceImpl): IWeatherDbService
}