package com.kindgeektestproject.app.di.components

import android.app.Application
import com.kindgeektestproject.app.App
import com.kindgeektestproject.app.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 12:55
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ServiceModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        ViewModelModule::class,
        InteractorModule::class,
        AndroidInjectionModule::class,
        ActivityBuildersModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun networkModule(networkModule: NetworkModule): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}