package com.kindgeektestproject.app.di.modules

import com.kindgeektestproject.app.fragments.MapFragment
import com.kindgeektestproject.app.fragments.WeatherInfoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 10:14
 */
@Module
abstract class MainFragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributeWeatherInfoFragment(): WeatherInfoFragment

    @ContributesAndroidInjector
    abstract fun contributeMapFragment(): MapFragment
}