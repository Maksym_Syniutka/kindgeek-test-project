package com.kindgeektestproject.app.di.modules

import com.kindgeektestproject.domain.implementations.WeatherInteractorImpl
import com.kindgeektestproject.domain.interfaces.IWeatherInteractor
import dagger.Binds
import dagger.Module

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:22
 */
@Module
abstract class InteractorModule {

    @Binds
    abstract fun bindWeatherInteractor(weatherInteractor: WeatherInteractorImpl): IWeatherInteractor
}