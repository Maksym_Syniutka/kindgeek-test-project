package com.kindgeektestproject.app.di.modules

import com.kindgeektestproject.app.fragments.authentication.SplashScreenFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 13:14
 */
@Module
abstract class AuthFragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashScreenFragment(): SplashScreenFragment
}