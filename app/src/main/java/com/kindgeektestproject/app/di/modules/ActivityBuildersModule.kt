package com.kindgeektestproject.app.di.modules

import com.kindgeektestproject.app.activity.AuthActivity
import com.kindgeektestproject.app.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 13:13
 */
@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = [AuthFragmentsModule::class])
    abstract fun contributeAuthActivity(): AuthActivity

    @ContributesAndroidInjector(modules = [MainFragmentsModule::class])
    abstract fun contributeMainActivity(): MainActivity
}