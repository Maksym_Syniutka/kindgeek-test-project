package com.kindgeektestproject.app.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 13:00
 */
@Module
class AppModule {

    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
}