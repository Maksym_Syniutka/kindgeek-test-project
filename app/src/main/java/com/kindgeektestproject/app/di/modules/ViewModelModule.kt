package com.kindgeektestproject.app.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kindgeektestproject.app.di.ViewModelKey
import com.kindgeektestproject.app.viewmodel.AppViewModelFactory
import com.kindgeektestproject.app.viewmodel.SharedViewModel
import com.kindgeektestproject.app.viewmodel.SplashScreenFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 13:08
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenFragmentViewModel::class)
    abstract fun bindSplashScreenFragmentViewModel(splashScreenFragmentViewModel: SplashScreenFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel::class)
    abstract fun bindSharedViewModel(sharedViewModel: SharedViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}