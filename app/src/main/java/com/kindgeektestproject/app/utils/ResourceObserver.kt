package com.kindgeektestproject.app.utils

import android.arch.lifecycle.Observer
import com.kindgeektestproject.data.entities.common.Resource
import com.kindgeektestproject.data.entities.common.State

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:19
 */
class ResourceObserver<T>(
    val onLoading: ((T?) -> Unit)? = null,
    val onSuccess: ((T?) -> Unit)? = null,
    val onError: ((Throwable?) -> Unit)? = null
) : Observer<Resource<T>> {
    override fun onChanged(t: Resource<T>?) {
        when (t?.state) {
            is State.ERROR -> onError?.invoke(t.state?.throwable)
            is State.LOADING -> onLoading?.invoke(t.data)
            is State.SUCCESS -> onSuccess?.invoke(t.data)
        }
    }
}