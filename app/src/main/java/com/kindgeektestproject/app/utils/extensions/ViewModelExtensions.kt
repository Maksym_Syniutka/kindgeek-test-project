package com.kindgeektestproject.app.utils.extensions

import android.arch.lifecycle.*
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.kindgeektestproject.app.activity.BaseActivity

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 14:06
 */
inline fun <reified T : ViewModel> Fragment.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

inline fun <reified T : ViewModel> Fragment.getSharedViewModel(
    activity: BaseActivity,
    viewModelFactory: ViewModelProvider.Factory
): T {
    return ViewModelProviders.of(activity, viewModelFactory).get(T::class.java)
}

inline fun <reified T : ViewModel> FragmentActivity.getSharedViewModel(
    activity: BaseActivity,
    viewModelFactory: ViewModelProvider.Factory
): T {
    return ViewModelProviders.of(activity, viewModelFactory).get(T::class.java)
}

/**
 * Example:
 * withViewModel<PostListViewModel>(viewModelFactory) {
 * observe(posts, ::updatePosts)
 * swipeRefreshLayout.setOnRefreshListener { get(refresh = true) }
}
 */
inline fun <reified T : ViewModel> Fragment.withViewModel(
    viewModelFactory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val vm = getViewModel<T>(viewModelFactory)
    vm.body()
    return vm
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}