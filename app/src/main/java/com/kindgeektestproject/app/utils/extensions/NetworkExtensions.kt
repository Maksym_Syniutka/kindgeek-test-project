package com.kindgeektestproject.app.utils.extensions

import android.Manifest
import android.content.Context
import android.net.ConnectivityManager
import android.support.annotation.RequiresPermission

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 23:54
 */

@RequiresPermission(value = Manifest.permission.ACCESS_NETWORK_STATE)
fun Context?.isConnected(): Boolean {
    if (this == null) return false

    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.activeNetworkInfo?.isConnected ?: false
}