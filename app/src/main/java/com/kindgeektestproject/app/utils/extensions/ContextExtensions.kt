package com.kindgeektestproject.app.utils.extensions

import android.app.Activity
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 14:32
 */
/**
 * Creates intent for specified [T] component of Android.
 *
 * @param action - value passed to [Intent.setAction]
 * @param flags - value passed to [Intent.setFlags]
 */
inline fun <reified T : Any> Context.intentFor(
    action: String? = null,
    flags: Int = -1
): Intent = Intent(this, T::class.java).apply {
    this.action = action
    this.flags = flags
}

/**
 * Creates intent for specified [T] component of Android and
 * initialize it with [init] block.
 */
inline fun <reified T : Any> Context.intentFor(
    action: String? = null,
    flags: Int = -1,
    init: Intent.() -> Unit
): Intent = intentFor<T>(action, flags).apply(init)

/**
 * Start activity for specified [T] Activity
 *
 * @param action - value passed to [Intent.setAction]
 * @param flags - value passed to [Intent.setFlags]
 */
inline fun <reified T : Activity> Context.startActivity(
    action: String? = null,
    flags: Int = -1
) = startActivity(intentFor<T>(action, flags))

/**
 * Start activity for specified [T] Activity
 *
 * @param action - value passed to [Intent.setAction]
 * @param flags - value passed to [Intent.setFlags]
 * @param init - function with [Intent] value as its receiver
 */
inline fun <reified T : Activity> Context.startActivity(
    action: String? = null,
    flags: Int = -1,
    init: Intent.() -> Unit
) = startActivity(intentFor<T>(action, flags, init))

/**
 * Start service for specified [T] Service
 *
 * @param action - value passed to [Intent.setAction]
 */
inline fun <reified T : Service> Context.startService(
    action: String? = null
): ComponentName {
    return if (isAndroidOreoOrHigher()) startForegroundService(intentFor<T>(action = action))
    else startService(intentFor<T>(action = action))
}

/**
 * Start service for specified [T] Service
 *
 * @param action - value passed to [Intent.setAction]
 * @param init - function with [Intent] value as its receiver
 */
inline fun <reified T : Service> Context.startService(
    action: String? = null,
    init: Intent.() -> Unit
): ComponentName {
    return if (isAndroidOreoOrHigher()) startForegroundService(intentFor<T>(action = action, init = init))
    else startService(intentFor<T>(action = action, init = init))
}

fun Activity.finishWithoutAnimation() {
    finish()
    overridePendingTransition(0, 0)
}

fun isAndroidOreoOrHigher(): Boolean = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O