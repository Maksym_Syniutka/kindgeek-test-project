package com.kindgeektestproject.app.utils

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.kindgeektestproject.data.exceptions.BaseThrowable
import com.kindgeektestproject.data.exceptions.UnknownError
import io.reactivex.exceptions.CompositeException
import timber.log.Timber

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 22:52
 */
object ErrorHandler {
    fun handleException(t: Throwable?): BaseThrowable {
        val error = when (t) {
            is HttpException -> when (t.code()) {
                503 -> UnknownError(rootCause = t) //Maintenance
                else -> UnknownError(rootCause = t)
            }
            is CompositeException -> handleException(t.exceptions[0])
            is BaseThrowable -> t
            else -> UnknownError(rootCause = t)
        }

        Timber.d("handling exception, received error: $t, handled error: $error")

        return error
    }
}