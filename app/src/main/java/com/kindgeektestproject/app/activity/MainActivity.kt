package com.kindgeektestproject.app.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.databinding.ActivityMainBinding
import com.kindgeektestproject.app.utils.extensions.getSharedViewModel
import com.kindgeektestproject.app.utils.extensions.isConnected
import com.kindgeektestproject.app.viewmodel.SharedViewModel
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : BaseActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setDataBindingContentView(R.layout.activity_main)
        binding.bottomNavigationBar.itemIconTintList = null
        binding.bottomNavigationBar.run {
            NavigationUI.setupWithNavController(
                this,
                (supportFragmentManager.findFragmentById(R.id.main_navigation_fragments) as NavHostFragment).navController
            )
        }

        if(!isConnected()) showSnackbar(R.string.main_activity_no_internet_connection_snackbar_message)

        viewModel = getSharedViewModel(this, viewModelFactory)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}
