package com.kindgeektestproject.app.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.databinding.ActivityAuthBinding
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class AuthActivity : BaseActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setDataBindingContentView(R.layout.activity_auth)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}