package com.kindgeektestproject.app.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.annotation.VisibleForTesting
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.kindgeektestproject.app.R
import com.kindgeektestproject.app.databinding.ActivityBaseBinding
import com.kindgeektestproject.app.viewmodel.BaseViewModel
import com.kindgeektestproject.data.entities.common.State
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 12:34
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var baseBinding: ActivityBaseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    fun <T : BaseViewModel> subscribeToViewModelEvents(viewModel: T) {
        Timber.d("subscribing to view model events, viewModel:$viewModel")
        viewModel.snackbarEvent.observe(this, Observer { showSnackbar(it) })
        viewModel.loadingEvent.observe(this, Observer { processResponseStatus(State.LOADING) })
        viewModel.successEvent.observe(this, Observer { processResponseStatus(State.SUCCESS) })
        viewModel.errorEvent.observe(this, Observer { processResponseStatus(State.ERROR(it)) })
    }

    fun showSnackbar(@StringRes stringRes: Int? = -1, message: String? = null) {
        require(stringRes != -1 || message != null) { "you need to define " }

        val messageToShow: String = if (stringRes == -1) message!! else getString(stringRes!!)

        Snackbar.make(baseBinding.root, messageToShow, Snackbar.LENGTH_LONG).show()
    }

    fun showLoading() {
        Timber.d("showing progress bar")
        baseBinding.baseActivityLoadProgressBar.visibility = View.VISIBLE
    }

    fun hideLoading() {
        Timber.d("hiding progress bar")
        baseBinding.baseActivityLoadProgressBar.visibility = View.GONE
    }

    fun processResponseStatus(state: State?) {
        Timber.d("processing response status: $state")
        baseBinding.state = state
    }

    protected fun <T : ViewDataBinding> setDataBindingContentView(@LayoutRes resId: Int): T {
        Timber.d("setting data binding content view")
        return DataBindingUtil.inflate(layoutInflater, resId, baseBinding.baseActivityBaseContentLayout, true)
    }
}

