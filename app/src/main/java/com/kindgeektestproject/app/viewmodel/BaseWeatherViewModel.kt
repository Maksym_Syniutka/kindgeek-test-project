package com.kindgeektestproject.app.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.kindgeektestproject.data.entities.common.Resource
import com.kindgeektestproject.data.entities.room.WeatherInfo
import com.kindgeektestproject.domain.interfaces.IWeatherInteractor

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 09:28
 */
abstract class BaseWeatherViewModel(
    protected val weatherInteractor: IWeatherInteractor
) : BaseViewModel() {
    val weatherInfoLiveData: LiveData<Resource<WeatherInfo>>
        get() = weatherMutableInfo

    protected val weatherMutableInfo = MutableLiveData<Resource<WeatherInfo>>()

    fun getWeatherInfo() {
        weatherInteractor.getWeatherInfo()
            .subscribe(createSingleObserver(weatherMutableInfo))
    }
}