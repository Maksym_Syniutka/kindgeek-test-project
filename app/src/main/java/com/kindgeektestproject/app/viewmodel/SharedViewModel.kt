package com.kindgeektestproject.app.viewmodel

import com.google.android.gms.maps.model.LatLng
import com.kindgeektestproject.domain.interfaces.IWeatherInteractor
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 09:06
 */
class SharedViewModel @Inject constructor(
    weatherInteractor: IWeatherInteractor
) : BaseWeatherViewModel(weatherInteractor) {

    fun updateWeatherInfo(latLng: LatLng) {
        weatherInteractor.getWeatherByLocation(latLng.latitude, latLng.longitude)
            .subscribe(createSingleObserver(weatherMutableInfo))
    }
}