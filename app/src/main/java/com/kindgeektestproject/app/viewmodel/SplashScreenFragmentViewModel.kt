package com.kindgeektestproject.app.viewmodel

import com.kindgeektestproject.domain.interfaces.IWeatherInteractor
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 14:04
 */
class SplashScreenFragmentViewModel @Inject constructor(
    weatherInteractor: IWeatherInteractor
) : BaseWeatherViewModel(weatherInteractor)