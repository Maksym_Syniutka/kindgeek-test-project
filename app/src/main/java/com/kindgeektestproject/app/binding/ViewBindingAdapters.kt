package com.kindgeektestproject.app.binding

import android.databinding.BindingAdapter
import android.view.View

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 12:37
 */

@BindingAdapter("bind:goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}