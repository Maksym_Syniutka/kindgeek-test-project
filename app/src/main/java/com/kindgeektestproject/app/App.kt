package com.kindgeektestproject.app

import android.app.Activity
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.kindgeektestproject.app.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 12:55
 */
class App : MultiDexApplication(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)

        initTimber()
    }

    override fun activityInjector() = dispatchingAndroidInjector

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    private class CrashReportingTree : DebugTree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority >= Log.INFO) super.log(priority, tag, message, t)
        }
    }

    private open class DebugTree : Timber.DebugTree() {
        override fun createStackElementTag(element: StackTraceElement): String? {
            return String.format("(%s:%s):%s ", element.fileName, element.lineNumber, element.methodName)
        }
    }
}