package com.kindgeektestproject.app

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.kindgeektestproject.data.db.WeatherDatabase
import com.kindgeektestproject.data.entities.room.WeatherInfo
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 11:43
 */
@RunWith(AndroidJUnit4::class)
class WeatherDaoTest {

    private lateinit var database: WeatherDatabase

    private val weatherData = WeatherInfo(
        "Lviv", 0.0, 0, "UA", "", 2.0,
        3.0, 4.0, 5.0, 1.0,
        2.0, 1.5, 4.5, "", "d", 3.2,
        4.5, 6, 6.5, 7.8, "UA", "", "",
        "", -4.5, "", 3.2, 4.5, 6.7, "",
        "", 1, 2.4
    )

    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears after test
        database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), WeatherDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun getWeatherWhenNoWeatherSaved() {
        val weather = database.weatherDao().getWeatherInfo()
        assertThat(weather).isNull()
    }

    @Test
    fun saveAndGetWeather() {
        database.weatherDao().saveWeatherInfo(weatherData)

        val weather = database.weatherDao().getWeatherInfo()
        assertThat(weather).isNotNull
        assertThat(weather).isEqualTo(weatherData)
    }

    @Test
    fun deleteAndGetUser() {
        database.weatherDao().saveWeatherInfo(weatherData)
        database.weatherDao().deleteData()

        val weather = database.weatherDao().getWeatherInfo()
        assertThat(weather).isNull()
    }

    @Test
    fun deleteAndGetUsers() {
        database.weatherDao().saveWeatherInfo(weatherData)
        val newWeather = WeatherInfo(
            "Kyiv", 0.0, 0, "UA", "", 2.0,
            3.0, 4.0, 5.0, 1.0,
            2.0, 1.5, 4.5, "", "d", 3.2,
            4.5, 6, 6.5, 7.8, "UA", "", "",
            "", -4.5, "", 3.2, 4.5, 6.7, "",
            "", 1, 2.4
        )
        database.weatherDao().saveWeatherInfo(newWeather)
        database.weatherDao().deleteData()

        val weather = database.weatherDao().getWeatherInfo()
        assertThat(weather).isNull()
    }

}