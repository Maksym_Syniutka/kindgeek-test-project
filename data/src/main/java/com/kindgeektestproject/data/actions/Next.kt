package com.kindgeektestproject.data.actions

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 23:07
 */
typealias Next<T> = (T) -> Unit