package com.kindgeektestproject.data.actions

import org.reactivestreams.Subscription

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 23:07
 */
typealias Subscribe = (Subscription?) -> Unit