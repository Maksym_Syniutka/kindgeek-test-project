package com.kindgeektestproject.data.roomutil

import android.arch.persistence.room.RoomDatabase
import com.kindgeektestproject.data.entities.room.RoomNullable
import com.kindgeektestproject.data.exceptions.RoomError
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.functions.Function

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:27
 */
object RoomObservable {

    fun <Data, Database : RoomDatabase> createObject(
        database: Database,
        function: Function<Database, Data>
    ): Flowable<Data> {
        return Flowable.create(object : OnSubscribeRoom<Data>(database) {
            override fun get(): Data? {
                return try {
                    function.apply(database)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
            }
        }, BackpressureStrategy.LATEST)
            .map {
                if (it.data == null) throw RoomError(message = "Data is null")
                else it.data
            }
    }

    fun <Data, Database : RoomDatabase> createNullableObject(
        klipDatabase: Database,
        function: Function<Database, Data>
    ): Flowable<RoomNullable<Data?>> {
        return Flowable.create(object : OnSubscribeRoom<Data>(klipDatabase) {
            override fun get(): Data? {
                return try {
                    function.apply(klipDatabase)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
            }
        }, BackpressureStrategy.LATEST)
    }
}