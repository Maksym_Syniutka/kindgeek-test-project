package com.kindgeektestproject.data.exceptions

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:05
 */
class WeatherInfoDoesNotExist : BaseThrowable()