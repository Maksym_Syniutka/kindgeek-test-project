package com.kindgeektestproject.data.exceptions

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:40
 */
class UnknownError(
    rootCause: Throwable?,
    message: String = ""
) : BaseThrowable(rootCause, message)