package com.kindgeektestproject.data.exceptions

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:39
 */
class RoomError(
    rootCause: Throwable = Throwable(),
    message: String = ""
) : BaseThrowable(rootCause, message)