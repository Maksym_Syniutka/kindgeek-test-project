package com.kindgeektestproject.data.exceptions

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:36
 */
abstract class BaseThrowable(
    public val rootCause: Throwable? = null,
    message: String = rootCause?.message ?: ""
) : Throwable(message, rootCause)