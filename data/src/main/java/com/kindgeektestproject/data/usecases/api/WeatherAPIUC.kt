package com.kindgeektestproject.data.usecases.api

import com.kindgeektestproject.data.entities.response.WeatherResponse
import com.kindgeektestproject.data.services.api.IWeatherApiService
import com.kindgeektestproject.data.usecases.BaseUseCase
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 10:41
 */
class WeatherAPIUC @Inject constructor(private val weatherApiService: IWeatherApiService) : BaseUseCase() {

    fun getWeatherByLocation(apiKey: String, latitude: Double, longitude: Double): Single<WeatherResponse> {
        return weatherApiService.getWeatherByLocation(apiKey, latitude, longitude)
    }
}