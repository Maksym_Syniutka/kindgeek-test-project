package com.kindgeektestproject.data.usecases.db

import com.kindgeektestproject.data.entities.room.WeatherInfo
import com.kindgeektestproject.data.services.db.IWeatherDbService
import com.kindgeektestproject.data.usecases.BaseUseCase
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:13
 */
@Singleton
class WeatherDBUC @Inject constructor(private val weatherDbService: IWeatherDbService) : BaseUseCase() {

    fun saveWeatherInfo(weatherInfo: WeatherInfo): Single<WeatherInfo> {
        return weatherDbService.saveWeatherInfo(weatherInfo)
    }

    fun getWeatherInfo(): Single<WeatherInfo> {
        return weatherDbService.getWeatherInfo()
    }

    fun clearData(): Single<Boolean> {
        return weatherDbService.clearData()
    }
}