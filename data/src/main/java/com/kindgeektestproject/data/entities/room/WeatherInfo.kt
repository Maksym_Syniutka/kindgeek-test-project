package com.kindgeektestproject.data.entities.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class WeatherInfo(
    @SerializedName("city_name")
    @PrimaryKey
    val cityName: String,
    @SerializedName("app_temp")
    val feelsLikeTemperature: Double,
    @SerializedName("clouds")
    val cloudCoverage: Int,
    @SerializedName("country_code")
    val countryCode: String,
    @SerializedName("datetime")
    val datetime: String,
    @SerializedName("dewpt")
    val dewPoint: Double,
    @SerializedName("dhi")
    val diffuseHorizontalSolarIrradiance: Double,
    @SerializedName("dni")
    val directNormalSolarIrradiance: Double,
    @SerializedName("elev_angle")
    val solarElevationAngle: Double,
    @SerializedName("ghi")
    val globalHorizontalSolarIrradiance: Double,
    @SerializedName("h_angle")
    val solarHourAngle: Double,
    @SerializedName("lat")
    val latitude: Double,
    @SerializedName("lon")
    val longitude: Double,
    @SerializedName("ob_time")
    val lastObservationTime: String,
    @SerializedName("pod")
    val partOfTheDay: String,
    @SerializedName("precip")
    val liquidEquivalentPrecipitationRate: Double,
    @SerializedName("pres")
    val pressure: Double,
    @SerializedName("rh")
    val relativeHumidity: Int,
    @SerializedName("slp")
    val seaLevelPressure: Double,
    @SerializedName("solar_rad")
    val estimatedSolarRadiation: Double,
    @SerializedName("state_code")
    val stateCode: String,
    @SerializedName("station")
    val station: String,
    @SerializedName("sunrise")
    val sunrise: String,
    @SerializedName("sunset")
    val sunset: String,
    @SerializedName("temp")
    val temperature: Double,
    @SerializedName("timezone")
    val timezone: String,
    @SerializedName("ts")
    val lastObservationTimestamp: Double,
    @SerializedName("uv")
    val uvIndex: Double,
    @SerializedName("vis")
    val visibility: Double,
    @SerializedName("wind_cdir")
    val abbreviatedWindDirection: String,
    @SerializedName("wind_cdir_full")
    val verbalWindDirection: String,
    @SerializedName("wind_dir")
    val windDirection: Int,
    @SerializedName("wind_spd")
    val windSpeed: Double
)