package com.kindgeektestproject.data.entities.common

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:51
 */
sealed class State(val throwable: Throwable? = null) {
    object LOADING : State()
    object SUCCESS : State()
    data class ERROR(private val t: Throwable?) : State(t)
}