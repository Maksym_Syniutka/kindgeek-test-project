package com.kindgeektestproject.data.entities.response

import com.google.gson.annotations.SerializedName
import com.kindgeektestproject.data.entities.room.WeatherInfo

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 10:57
 */
data class WeatherResponse(
    @SerializedName("data") val data: List<WeatherInfo>
)