package com.kindgeektestproject.data.entities.room

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:22
 */
data class RoomNullable<T>(var data: T?)