package com.kindgeektestproject.data.db

import android.arch.persistence.room.*
import com.kindgeektestproject.data.entities.room.WeatherInfo

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 15:23
 */
@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveWeatherInfo(weatherInfo: WeatherInfo)

    @Query("SELECT * FROM WeatherInfo")
    fun getWeatherInfo(): WeatherInfo?

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateWeatherInfo(weatherInfo: WeatherInfo)

    @Query("DELETE FROM WeatherInfo")
    fun deleteData()
}