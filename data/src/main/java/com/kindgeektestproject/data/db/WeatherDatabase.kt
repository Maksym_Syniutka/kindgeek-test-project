package com.kindgeektestproject.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.kindgeektestproject.data.entities.room.WeatherInfo

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 15:24
 */
@Database(
    entities = [
        WeatherInfo::class
    ],
    version = 1,
    exportSchema = false
)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao
}