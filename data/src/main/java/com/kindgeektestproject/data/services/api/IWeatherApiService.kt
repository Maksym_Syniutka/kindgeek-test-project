package com.kindgeektestproject.data.services.api

import com.kindgeektestproject.data.entities.response.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Maksym Syniutka
 *         Date: Mon 28-Jan-2019
 *         Time: 10:36
 */
interface IWeatherApiService {

    @GET("current")
    fun getWeatherByLocation(
        @Query("key") apiKey: String,
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double
    ): Single<WeatherResponse>
}