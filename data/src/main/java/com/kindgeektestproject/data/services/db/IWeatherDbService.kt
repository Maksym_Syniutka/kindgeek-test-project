package com.kindgeektestproject.data.services.db

import com.kindgeektestproject.data.entities.room.WeatherInfo
import io.reactivex.Single

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 15:55
 */
interface IWeatherDbService {

    fun saveWeatherInfo(weatherInfo: WeatherInfo): Single<WeatherInfo>

    fun getWeatherInfo(): Single<WeatherInfo>

    fun clearData(): Single<Boolean>
}