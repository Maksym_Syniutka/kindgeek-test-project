package com.kindgeektestproject.data.services.db

import com.kindgeektestproject.data.db.WeatherDatabase
import com.kindgeektestproject.data.entities.room.WeatherInfo
import com.kindgeektestproject.data.exceptions.WeatherInfoDoesNotExist
import com.kindgeektestproject.data.roomutil.RoomObservable
import com.kindgeektestproject.utilities.SchedulersFacade
import io.reactivex.Single
import io.reactivex.functions.Function
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 15:57
 */
@Singleton
class WeatherDbServiceImpl @Inject constructor(
    private val weatherDatabase: WeatherDatabase,
    private val schedulers: SchedulersFacade
) : IWeatherDbService {

    override fun saveWeatherInfo(weatherInfo: WeatherInfo): Single<WeatherInfo> = RoomObservable.createObject(
        weatherDatabase,
        Function<WeatherDatabase, WeatherInfo> {
            it.weatherDao().saveWeatherInfo(weatherInfo)
            return@Function weatherInfo
        }
    ).firstOrError().subscribeOn(schedulers.io())

    override fun getWeatherInfo(): Single<WeatherInfo> = RoomObservable.createObject(
        weatherDatabase,
        Function<WeatherDatabase, WeatherInfo> {
            it.weatherDao().getWeatherInfo() ?: throw WeatherInfoDoesNotExist()
        }
    ).firstOrError().subscribeOn(schedulers.io())

    override fun clearData(): Single<Boolean> = RoomObservable.createObject(
        weatherDatabase,
        Function<WeatherDatabase, Boolean> {
            it.weatherDao().deleteData()

            return@Function true
        }
    ).firstOrError().subscribeOn(schedulers.io())

}