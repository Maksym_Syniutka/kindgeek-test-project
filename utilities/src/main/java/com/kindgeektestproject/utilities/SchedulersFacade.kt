package com.kindgeektestproject.utilities

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 15:58
 */
@Singleton
class SchedulersFacade @Inject constructor() {

    /**
     * IO thread pool scheduler
     */
    fun io(): Scheduler = Schedulers.io()

    /**
     * Computation thread pool scheduler
     */
    fun computation(): Scheduler = Schedulers.computation()

    /**
     * Main Thread scheduler
     */
    fun ui(): Scheduler = AndroidSchedulers.mainThread()
}