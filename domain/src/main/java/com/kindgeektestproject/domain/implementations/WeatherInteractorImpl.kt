package com.kindgeektestproject.domain.implementations

import com.kindgeektestproject.data.entities.response.WeatherResponse
import com.kindgeektestproject.data.entities.room.WeatherInfo
import com.kindgeektestproject.data.usecases.api.WeatherAPIUC
import com.kindgeektestproject.data.usecases.db.WeatherDBUC
import com.kindgeektestproject.domain.interfaces.IWeatherInteractor
import com.kindgeektestproject.utilities.Constants
import com.kindgeektestproject.utilities.SchedulersFacade
import io.reactivex.Single
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:12
 */
@Singleton
class WeatherInteractorImpl @Inject constructor(
    schedulers: SchedulersFacade,
    private val weatherDBUC: WeatherDBUC,
    private val weatherAPIUC: WeatherAPIUC
) : BaseInteractorImpl(schedulers), IWeatherInteractor {

    override fun getWeatherInfo(): Single<WeatherInfo> {
        return weatherDBUC.getWeatherInfo()
            .flatMap { weatherAPIUC.getWeatherByLocation(Constants.WEATHER_API_KEY, it.latitude, it.longitude) }
            .onErrorResumeNext { throwable ->
                if (throwable is UnknownHostException) {
                    return@onErrorResumeNext weatherDBUC.getWeatherInfo().flatMap {
                        Single.just(WeatherResponse(listOf(it)))
                    }
                } else {
                    throw throwable
                }
            }
            .flatMap { weather -> weatherDBUC.clearData().flatMap { Single.just(weather) } }
            .flatMap { weatherDBUC.saveWeatherInfo(it.data[0]) }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    override fun getWeatherByLocation(latitude: Double, longitude: Double): Single<WeatherInfo> {
        return weatherAPIUC.getWeatherByLocation(Constants.WEATHER_API_KEY, latitude, longitude)
            .map { it.data[0] }
            .flatMap { weather -> weatherDBUC.clearData().flatMap { Single.just(weather) } }
            .flatMap { weatherDBUC.saveWeatherInfo(it) }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }
}