package com.kindgeektestproject.domain.implementations

import com.kindgeektestproject.domain.interfaces.IBaseInteractor
import com.kindgeektestproject.utilities.SchedulersFacade

/**
 * @author Maksym Syniutka
 *         Date: Sat 26-Jan-2019
 *         Time: 16:02
 */
abstract class BaseInteractorImpl(
    protected val schedulers: SchedulersFacade
) : IBaseInteractor