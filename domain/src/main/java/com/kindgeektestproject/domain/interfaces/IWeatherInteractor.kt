package com.kindgeektestproject.domain.interfaces

import com.kindgeektestproject.data.entities.room.WeatherInfo
import io.reactivex.Single

/**
 * @author Maksym Syniutka
 *         Date: Sun 27-Jan-2019
 *         Time: 16:11
 */
interface IWeatherInteractor {
    fun getWeatherInfo(): Single<WeatherInfo>

    fun getWeatherByLocation(latitude: Double, longitude: Double): Single<WeatherInfo>
}